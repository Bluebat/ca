<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Hotel;
use App\Repository\Contracts\HotelRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Hotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hotel[]    findAll()
 * @method Hotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class HotelRepository extends ServiceEntityRepository implements HotelRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hotel::class);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        $result = [];
        $hotels = $this->findAll();
        if (empty($hotels)) {
            return $result;
        }
        foreach ($hotels as $hotel) {
            $result[] = [
                'id' => $hotel->getId(),
                'name' => $hotel->getName(),
                'address' => $hotel->getAddress()
            ];
        }

        return $result;
    }
}
