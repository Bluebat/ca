<?php

namespace App\Repository;

use App\Entity\Review;
use App\Repository\Contracts\ReviewRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ReviewRepository extends ServiceEntityRepository implements ReviewRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    /**
     * @param int $hotelId
     * @return float
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function avgNote(int $hotelId): float
    {
        $avg = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('avg(review.score) as score')
            ->from(Review::class, 'review')
            ->where('review.hotel_id = :id')
            ->setParameter('id', $hotelId)
            ->getQuery()
            ->getSingleResult();

        return (float) $avg['score'];
    }

    /**
     * @return array
     */
    public function all(): array
    {
        $result = [];
        $reviews = $this->findAll();
        if (empty($reviews)) {
            return $result;
        }
        foreach ($reviews as $review) {
            $result[] = [
                'id' => $review->getId(),
                'hotelId' => $review->getHotelId(),
                'score' => $review->getScore(),
                'comment' => $review->getComment()
            ];
        }

        return $result;
    }

    /**
     * @param int $hotelId
     * @return array
     */
    public function byHotelId(int $hotelId): array
    {
        $result = [];
        $reviews = $this->findBy(['hotel_id' => $hotelId]);
        if (empty($reviews)) {
            return $result;
        }
        foreach ($reviews as $review) {
            $result[] = [
                'id' => $review->getId(),
                'hotelId' => $review->getHotelId(),
                'score' => $review->getScore(),
                'comment' => $review->getComment()
            ];
        }

        return $result;
    }
}
