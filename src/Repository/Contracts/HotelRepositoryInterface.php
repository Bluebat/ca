<?php

declare(strict_types=1);

namespace App\Repository\Contracts;

interface HotelRepositoryInterface
{
    /**
     * @return array
     */
    public function all(): array;
}
