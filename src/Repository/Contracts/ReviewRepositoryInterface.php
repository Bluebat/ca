<?php

declare(strict_types=1);

namespace App\Repository\Contracts;

interface ReviewRepositoryInterface
{
    /**
     * @param int $hotelId
     * @return float
     */
    public function avgNote(int $hotelId): float;

    /**
     * @return array
     */
    public function all(): array ;

    /**
     * @param int $hotelId
     * @return array
     */
    public function byHotelId(int $hotelId): array;
}
