<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\Contracts\HotelRepositoryInterface;
use App\Repository\Contracts\ReviewRepositoryInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ApiController extends AbstractController
{
    /**
     * @var HotelRepositoryInterface $hotelRepository
     */
    private HotelRepositoryInterface $hotelRepository;

    /**
     * @var ReviewRepositoryInterface
     */
    private ReviewRepositoryInterface $reviewRepository;

    public function __construct(
        HotelRepositoryInterface $hotelRepository,
        ReviewRepositoryInterface $reviewRepository
    ) {
        $this->hotelRepository = $hotelRepository;
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @Route("/api/average", name="average")
     * @param Request $request
     * @return Response
     */
    public function getAverage(Request $request): Response
    {
        $hotelId = (int) $request->get('hotelId');
        if (empty($hotelId)) {
            throw new RuntimeException('Hotel not found.');
        }

        return new JsonResponse($this->reviewRepository->avgNote($hotelId));
    }

    /**
     * @Route("/api/reviews", name="review_list")
     */
    public function getReviews(Request $request)
    {
        $hotelId = (int) $request->get('hotelId');
        if (empty($hotelId)) {
            return new JsonResponse($this->reviewRepository->all());
        }
        return new JsonResponse($this->reviewRepository->byHotelId($hotelId));
    }

    /**
     * @Route("/api/hotels", name="hotel_list")
     */
    public function getHotels()
    {
        return new JsonResponse($this->hotelRepository->all());
    }
}
