<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class WelcomeController extends AbstractController
{
    /**
     * @Route("/", name="welcome")
     * @return Response
     */
    public function welcome(): Response
    {
        return $this->render('base.html.twig');
    }
}
